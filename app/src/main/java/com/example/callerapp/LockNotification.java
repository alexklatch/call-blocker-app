package com.example.callerapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RemoteViews;

import static com.example.callerapp.App.CHANNEL_ID;


public class LockNotification {
    private Context ctx;
    private String pkgName;

    private NotificationManagerCompat notificationManager;
    private Notification notification;
    private String showingText = "no number";
    private final String TAG = "LockNotification";

    public LockNotification(Context ctx, String pkgName) {
        this.ctx = ctx;
        this.pkgName = pkgName;
        notificationManager = NotificationManagerCompat.from(ctx);
        Intent popUpIntent = new Intent(ctx, PopUpActivity.class);
        popUpIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        popUpIntent.setType(String.valueOf(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT));
        RemoteViews collapsedView =
                new RemoteViews(pkgName, R.layout.lock_notification);
        RemoteViews expandedView =
                new RemoteViews(pkgName, R.layout.lock_notification_expanded);
        PendingIntent contentIntent =
                PendingIntent.getActivity(ctx, 0, popUpIntent, 0);


        expandedView.setOnClickPendingIntent(R.id.showPopup, contentIntent);
        collapsedView.setTextViewText(R.id.phone_number, showingText);


        notification = new NotificationCompat.Builder(ctx, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setPriority(5)
                .setDefaults(Notification.DEFAULT_ALL).build();
    }

    public void setShowingText(String showingText) {
        this.showingText = showingText;
    }

    public void showNotification() {
        notificationManager.notify(1, notification);
        Log.d(TAG, "Showed");
    }

}
