package com.example.callerapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class CallListener extends Service {
    private CallStateDetector callStateDetector;
    private final String TAG = "Service";

    public CallListener() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        callStateDetector = new CallStateDetector(this, getPackageName());
        Log.d(TAG, "Start BrCast");
        int res = super.onStartCommand(intent, flags, startId);
        callStateDetector.start();
        return res;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callStateDetector.stop();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
